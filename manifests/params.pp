class setup_zbx_agent::params {
  $zbx_package	= "zabbix-agent"
  $zbx_server   = 'localhost'
  $zbx_port     = '10050'
  $zbx_host     = "${facts['networking']['fqdn']}"
  $release_name	= "${facts['os']['distro']['codename']}"  

  case "${facts['os']['family']}" {
    'Debian': {
      $zbx_repo_temp	= 'zabbix_ubuntu_repo'
      $zbx_repo_path	= '/etc/apt/sources.list.d/zabbix.list'
      $fwd_type		= "ufw"
      $fwd_command	= "ufw allow ${zbx_port}"
      $fwd_command_pre	= ""
      $fwd_command_path	= "/usr/sbin/"
    }
    'RedHat': {
      $zbx_repo_temp	= 'zabbix_rhel_repo'
      $zbx_repo_path	= '/etc/yum.repos.d/zabbix.repo'
      $fwd_type		= "firewalld"
      $fwd_command	= "firewall-cmd --add-port=${zbx_port}/tcp --permanent && firewall-cmd --reload"
      $fwd_command_pre	= ''
      $fwd_command_path	= "/bin/"
    }
  }
}
