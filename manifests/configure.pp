class setup_zbx_agent::configure {
  file { '/etc/zabbix/zabbix_agentd.conf':
    content     => template('setup_zbx_agent/zabbix_agentd.erb'),
    owner       => 'zabbix',
    group       => 'zabbix',
  }

  service { 'zabbix-agent':
    ensure      => 'running',
    subscribe   => File['/etc/zabbix/zabbix_agentd.conf']
  }
}
