class setup_zbx_agent::install {
  file { $setup_zbx_agent::zbx_repo_path:
    content     => template("setup_zbx_agent/${setup_zbx_agent::zbx_repo_temp}.erb")
  }

  package { "$setup_zbx_agent::zbx_package":
    ensure      => 'present',
    require     => File["${setup_zbx_agent::zbx_repo_path}"],
  }

  exec { "$setup_zbx_agent::fwd_type":
    path        => "${setup_zbx_agent::fwd_command_path}",
    command     => "${setup_zbx_agent::fwd_command}",
    require     => Package["${setup_zbx_agent::zbx_package}"],
  }
}
