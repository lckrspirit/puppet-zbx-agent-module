class setup_zbx_agent (
  # Zabbix agent package:
  $zbx_package		= 'zabbix-agent',
  
  # Zabbix config parameters: 
  $zbx_server   	= $setup_zbx_agent::params::zbx_server,
  $zbx_port		= $setup_zbx_agent::params::zbx_port,
  $zbx_host     	= $setup_zbx_agent::params::zbx_host,
  
  # Zabbix repo path:
  $zbx_repo_path	= $setup_zbx_agent::params::zbx_repo_path,
  $zbx_repo_temp	= $setup_zbx_agent::params::zbx_repo_temp,
  
  # Host firewall settings:
  $fwd_type		= $setup_zbx_agent::params::fwd_type,
  $fwd_command		= $setup_zbx_agent::params::fwd_command,
  $fwd_command_path	= $setup_zbx_agent::params::fwd_command_path,

) inherits setup_zbx_agent::params {

  class {'setup_zbx_agent::install':} ->
  class {'setup_zbx_agent::configure':}

}
